**SDV WG Q1/Community Days**

When:  19.03-20.03 

Registration:
Register [here](https://eclipsecon.regfox.com/sdv-community-day-graz-2024)

Venue: 
HELMUT LIST HALLE - see [website]( https://www.helmut-list-halle.com/ )

Address: 
Waagner-Biro-Straße 98a
8020 Graz, Österreich

Contacts: 
info@helmut-list-halle.com
t +43 316 58 42 60

**Day 1: March 19th**

Program runs in hybrid form: 14:00-18:00

Evening meet & greet

| Time | Session Name | Presenter | 
| ------ | ------ | ------ | 
| 13:00-14:00 |Registration|  
| 14:00-14:15 | [Hybrid] Welcome and opening remarks| EF SDV Team and local host |
| 14:15-16:00 | [Hybrid] **Tech Talks & Project updates - 1h45m hours**  |
| 14:15-14:45 | Tech Talk: Software-defined Vehicle: How to Transform the Established Hardware-Centric Development Process | Peter Ebner,  Wolfgang Puntigam (AVL) |
| 14:45-15:15 | SDV Process & Maturity Badges - kickoff | Dana Vede (EF) |
| 15:15-15:35 | Tech Talk: Looking from the other side - SDV from a cloud perspective | Jan Jongen (Valtech Mobility) |
| 15:35-16:00 | New Project: Eclipse SDV Blueprint: Insurance | Mario Ortegon (Microsoft), Jannis de Veer, Ambrogio Tattolo (HDI) | 
| 16:00-16:30 | **Coffee Break - 0.5 hours** | 
| 16:30-18:00 | [Hybrid] **presentations - 1.5 hour** | 
| 16:30-17:00 | New Project: Eclipse Symphony | Charlie Lagervik & Haishi Bai  (Microsoft) |
| 17:00-17:30 | Project Update: Eclipse Ankaios | Kaloyan Rusev (Elektrobit), Oliver Klapper (Elektrobit)| 
| 17:30-18:00 | Project Update: OpenDUT | Reimar Stier (Mercedes-Benz Tech Innovation),  Stefan Marksteiner (AVL) | 
| 18:00-19:00 | [on site only] Meet & Greet Reception | 
 
**Day 2: March 20th**

Morning workshop (on site only) 9:00-13:00
Afternoon runs in hybrid form: 13:00-15:30


| Time | Session Name | Presenter | 
| ------ | ------ | ------ | 
| 8:00-9:00 |Registration & Coffee|  
| 9:00-11:30 | [on-site only] **open collaboration 2.5 Hours** |
| track 1. | Eclipse SDV Insurance Blueprint Hands on Workshop | Mario Ortegon (Microsoft), Jannis de Veer, Ambrogio Tattolo (HDI) |
| 9-10 track 2.1 | Eclipse SDV Orchestration Scenarios: Software Orchestration Blueprint updates, Eclipse Symphony and Toolchain Orchestration blueprint |  Oliver Klapper (Elektrobit), Leonardo Rossetti (RedHat), Lauren Datz, Haishi Bai, Filipe Prezado (Microsoft)|
| 10:00-10:30 | **Coffee Break - 0.5 hours** | 
| 10:30- 11:30 track 2.2 | SDV Process hands-on Workshop: Setting up project configuration files | Dana Vede (EF), Kai Hudalla, Daniel Krippner (ETAS), Holger Dormann (Elektrobit) | 
| 11:30-12:00 | [on-site only] **open collaboration wrap up 0.5 hours** | 
| 12:00-13:00 | **Lunch break 1 hour** |
| 13:00-14:30 | [Hybrid] **presentations - 1.5  hour** | 
| 13:00-13:30 | Cross organization activities: EU updates FEDERATE & HAL4SDV, SDV Alliance | Daniel Krippner (ETAS)  |
| 13:30-14:00 | Modernizing automotive toolchains with metadata driven orchestration blueprint (proposal)  | Gianluca Vitale (AVL), Filipe Prezado and Charlie Lagervik (Microsoft)  
| 14:00-14:30 | Security talk: Updates to the project self-service and outlining security best practices for your project | Thomas Neidhart (EF) |
| 14:30-15:00 | Coffee Break |
| 15:00-15:30 | [Hybrid] Event wrap up and closing remarks |


Open points:

- [ ] Rust: check in with Florian Giltcher on a Rust compiler talk - POSTPONE
- [ ] Frederic Desbiens (Eclipse Foundation) - Eclipse ThreadX - POSTPONE
- [ ] Eclipse Theia - practical usage example / demo for SDV community - POSTPONE
- [ ] Eclipse Capella - practical usage example / demo for SDV community - POSTPONE
- [ ] Possible new project proposal Eclipse Zenoh-Flow - Phani Gangula (Zettascale) - POSTPONE
- [ ] Introduction to FEDERATE - Peter Priller (AVL) - Combine with Daniel's presentation 



